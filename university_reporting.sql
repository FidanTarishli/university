--All data 

select * from  uni_universities u 
       join uni_faculties f on f.uni_university_id=u.id
       join uni_groups g on g.uni_faculty_id=f.id
       join uni_groups_teachers gt on gt.uni_groups_id=g.id
       join uni_students s on  s.uni_groups_id=g.id
       join uni_teachers t  on  t.id=gt.uni_teachers_id
       join uni_subjects  sb on sb.id=t.uni_subjects_id;
      
       
--Sql query for showing student's data along with his/her group and faculty name:

select s.id,s.first_name,s.last_name,s.birthdate,f.name,g.name
from uni_students s
join uni_groups g on s.uni_groups_id=g.id
join uni_faculties f on g.uni_faculty_id=f.id;

--1.Student Reports 
--Total Students: A report to display the total number of students:
select count(id)as student_count from uni_students;
select count(*) as student_count from uni_students;

--Students per Group: A report to display the number of Students for each group:
select g.name as gorup_name, count(s.id) as student_count 
from uni_students s
join uni_groups g
on s.uni_groups_id=g.id
group by g.name;

--2. Subjects Reports
--Most Popular Subjects: A report to display the Subjects that have been taught the most by each teacher:
select su.name as subject_name, count(t.id) as teacher_count
from uni_subjects su
join uni_teachers t
on su.id=t.uni_subjects_id
group by su.name
order by 2 desc
fetch first 3 rows only;


--Subjects per Teacher: A report to show the number of different subjects provided by each teacher:
select t.first_name as teacher_name, count(su.id) as subject_count
from uni_subjects su
join uni_teachers t
on su.id=t.uni_subjects_id
group by t.first_name
order by 2 desc;


--3. Teachers Reports
--Teachers per Faculty: A report to show the Teacher(s) for each Faculty:
select f.name as faculty_name, t.first_name as teacher_name
from uni_faculties f
join uni_groups g on f.id=g.uni_faculty_id
join uni_groups_teachers gt on gt.uni_groups_id=g.id
join uni_teachers t on t.id=gt.uni_teachers_id;


--4. Faculty Reports
--Most Active Faculties: A report to show the Faculties with the most Students studied at:
select f.name as faculty_name, count(s.id) as student_count
from uni_faculties f 
join uni_groups g on g.uni_faculty_id=f.id
join uni_students s on s.uni_groups_id=g.id
group by f.name;

        



